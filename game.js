/* 
   Vince Messenger
   CS413 Project 1 - Minimalism
   5/18/16
   Annoying Little Brother
*/


(function(){var gameport = document.getElementById("gameport");

var running = false;  // boolean flag for game running
var first_run = true;

var renderer = PIXI.autoDetectRenderer(400, 400, {backgroundColor: 0x5b6ee1});
gameport.appendChild(renderer.view);

var stage = new PIXI.Container();

// add background image
var bg = new PIXI.Sprite(new PIXI.Texture.fromImage("background.png"));
stage.addChild(bg);

var texture = PIXI.Texture.fromImage("smiley.png");
var bro_texture = PIXI.Texture.fromImage("little_bro.png");

var sprite = new PIXI.Sprite(texture);
sprite.anchor.x = 0.5;
sprite.anchor.y = 0.5;

var lil_bro = new PIXI.Sprite(bro_texture);
lil_bro.anchor.x = .5;
lil_bro.anchor.y = .5;

stage.addChild(sprite);
stage.addChild(lil_bro);

// create game over screen
var game_over = new PIXI.Text("GAME OVER");
game_over.position.x = 200;
game_over.position.y = 100;
var press_enter = new PIXI.Text("Press enter to restart", {font:"bold 15px Arial"});
press_enter.position.x = 207;
press_enter.position.y = 150;
game_over.visible = false;
press_enter.visible = false;
stage.addChild(game_over);
stage.addChild(press_enter);

// create start screen
var start_screen = new PIXI.Text("Press Enter to Play");
start_screen.position.x = 90;
start_screen.position.y = 100;
stage.addChild(start_screen);

// vars regarding the timer
var count = 0;
var timer = new PIXI.Text(count);
timer.position.x = 190;
timer.position.y = 3;
stage.addChild(timer);
var time;  // initialize clock at 0

// vars for processInput()
var goRight, goLeft, goUp, goDown;

// vars for bro movement
var bro_speed = .3, broUp, broDown, broLeft, broRight;

sprite.position.x = 200;
sprite.position.y = 200;

lil_bro.position.x = 50;
lil_bro.position.y = 50;

stage.addChild(sprite);
stage.addChild(lil_bro);

function init() {
	// timer init
	count = 0;
	timer.text = count;
	time = 0;

	// sprite init
    sprite.position.x = 200;
	sprite.position.y = 200;
	sprite.rotation = 0;

	lil_bro.position.x = 50;
	lil_bro.position.y = 50;
	lil_bro.rotation = 0;
	bro_speed = .3;

	running = true;  // restart game

	// game over screen reset
	game_over.visible = false;
	press_enter.visible = false;

	// start screen reset
	start_screen.visible = false;
}

// define keydownListener
function keydownListener(key) {

	if (key.keyCode == 65) {   // a key
		goLeft = true;
		//sprite.position.x -= 5;
	}

	if (key.keyCode == 68) {   // d key
		goRight = true;
		//sprite.position.x += 5;
	}

	if (key.keyCode == 87) {   // w key
		goUp = true;
	}

	if (key.keyCode == 83) {   // s key
		goDown = true;
	}

	if (key.keyCode == 13 && !running) {   // enter key
		init();   // restart game 
	}
}

// define keyupListener
function keyupListener(key) {
		if (key.keyCode == 65) {   // a key
		goLeft = false;
	}

	if (key.keyCode == 68) {   // d key
		goRight = false;
	}

	if (key.keyCode == 87) {   // w key
		goUp = false;
	}

	if (key.keyCode == 83) {   // s key
		goDown = false;
	}
}

function game() {
    if (running) {

	    if (!time){      // get start time
	    	time = new Date().getTime();
	    }

		processInput();
		moveBro();
		checkGameOver();
		checkClock();
	}
}

function checkClock() {
	if (new Date().getTime() - time >= 1000){
		timer.text = count;
		time = new Date().getTime();

		if (count % 10 == 0  && count > 0) {
			bro_speed += .5;   // increase bro speed every ten seconds
		}

		count++; // increment counter
	}

}

function processInput() {
	if (goLeft && sprite.position.x > 30) {
			sprite.position.x -= 3;
	}

	if (goRight && sprite.position.x < 400-30) {
		sprite.position.x += 3;
	}

	if (goUp && sprite.position.y > 30) {
		sprite.position.y -= 3;
	}

	if (goDown && sprite.position.y < 400 - 42) {
		sprite.position.y += 3;
	}

    // figure out bro movement
    if (lil_bro.position.x < sprite.position.x) {
    	broRight = true;
    	broLeft = false;
    }
    else {
    	broLeft = true;
    	broRight = false;
    }

    if (lil_bro.position.y < sprite.position.y) {
    	broDown = true;
    	broUp = false;
    }
    else {
    	broUp = true;
    	broDown = false;
    }
}

function moveBro() {
	if (broUp) {
		lil_bro.position.y -= bro_speed;
	}

	if (broDown) {
		lil_bro.position.y += bro_speed;
	}

	if (broRight) {
		lil_bro.position.x += bro_speed;
	}

	if (broLeft) {
		lil_bro.position.x -= bro_speed;
	}
}

function checkGameOver() {
	//console.log(Math.abs(sprite.position.x - lil_bro.position.x));
	if (Math.abs(sprite.position.x - lil_bro.position.x) < 50) {
		if (Math.abs(sprite.position.y - lil_bro.position.y) < 50) {
			game_over.visible = true;
			press_enter.visible = true;
			running = false;
		}
	}
}

// add event listeners
document.addEventListener('keydown', keydownListener);
document.addEventListener('keyup', keyupListener);

function animate() {
	requestAnimationFrame(animate);

    if (!running) {   // make characters spin on game over
    	sprite.rotation += .1;
    	lil_bro.rotation -= .1;
    }

	renderer.render(stage);
}

function update() {
	setInterval(game, 15);
}

animate();
update();
})();
